class ConceptsController < ApplicationController
  # GET /concepts
  def index
    @concepts = Concept.all
  end

  # GET /concepts/1
  def show
    @concept = Concept.find(params[:id])
  end

  # GET /concepts/new
  def new
    authorize! :create, Concept
    @concept = Concept.new
  end

  # POST /concepts
  def create
    authorize! :create, Concept
    @concept = Concept.new(concept_params)
    @concept.user = current_user

    respond_to do |format|
      if @concept.save
        format.html { redirect_to @concept, notice: 'Concept was successfully created.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # GET /concepts/1/edit
  def edit
    @concept = Concept.find(params[:id])
    authorize! :manage, @concept
  end

  # PATCH/PUT /concepts/1
  def update
    @concept = Concept.find(params[:id])
    authorize! :manage, @concept
    
    respond_to do |format|
      if @concept.update(concept_params)
        format.html { redirect_to @concept, notice: 'Concept was updated.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # DELETE /concepts/1
  def destroy
    @concept = Concept.find(params[:id])
    authorize! :manage, @concept
    @concept.destroy
    
    respond_to do |format|
      format.html { redirect_to concepts_url, notice: 'Concept was deleted.' }
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def concept_params
      params.require(:concept).permit(:name)
    end
end
