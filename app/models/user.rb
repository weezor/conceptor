class User < ActiveRecord::Base
  has_secure_password
  has_many :concepts
  
  #validates_uniqueness_of :email
  validates_uniqueness_of :name
  validates_length_of :password, within: 3..32
  #validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  validates_format_of :name, with: /[a-zA-Z0-9]+/, message: "only allows letters"
  validates_format_of :password, with: /[a-zA-Z0-9]+/, message: "only allows letters"
  validates_length_of :name, within: 3..32, message: "name lenght between 3 - 32"
end
