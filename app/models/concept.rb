class Concept < ActiveRecord::Base
  belongs_to :user
  
  validates_length_of :name, within: 3..120
end
